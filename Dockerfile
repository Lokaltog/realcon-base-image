FROM debian:jessie
FROM python:3.4
MAINTAINER Kim Silkebækken <kim@dyktig.as>

# Install build deps
RUN gpg --keyserver pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4
RUN apt-get -y update && apt-get install -y curl

# Install gosu
RUN curl -o /usr/local/bin/gosu -SL "https://github.com/tianon/gosu/releases/download/1.3/gosu-$(dpkg --print-architecture)" \
	&& curl -o /usr/local/bin/gosu.asc -SL "https://github.com/tianon/gosu/releases/download/1.3/gosu-$(dpkg --print-architecture).asc" \
	&& gpg --verify /usr/local/bin/gosu.asc \
	&& rm /usr/local/bin/gosu.asc \
	&& chmod +x /usr/local/bin/gosu

# Install node
RUN apt-get install -y nodejs-legacy \
	&& curl -L --insecure -O https://www.npmjs.org/install.sh \
	&& /bin/bash install.sh

# Install application deps
RUN apt-get install -y git pkg-config build-essential libpq-dev libssl-dev libffi-dev python-dev
RUN apt-get install -y postgresql-client supervisor varnish nginx

RUN pip install --upgrade pip
RUN pip install circus==0.11.1

# Cleanup image
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Add repo hosts to known_hosts
RUN mkdir /root/.ssh
RUN ssh-keyscan bitbucket.org github.com > /etc/ssh/ssh_known_hosts

# Add woff2 to mimetypes
RUN echo "application/font-woff2 woff2" >> /etc/mime.types
